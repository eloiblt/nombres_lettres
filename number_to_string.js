function saisie_nb_entre_bornes()
{
    var nb = "";
    while (true) 
    {
        while (isNaN(parseInt(nb, 10))) 
        {
            nb = prompt('Entrez un nombre entre 0 et 9999 :');
            if (nb == null) {
                return -1; 
            }
        }
        if (nb >= 0 && nb <= 9999)
            break;
        else
            nb = "";
    }
    return nb;
}

function miliers(nb,nb1,nb2,nb3)
{
    nb = parseInt(nb);
    switch (nb) 
    {
        case 1:
            if (nb1 == 0 && nb2 == 0 && nb3 == 0)
                return "mille";
            else 
                return "mille-"
        case 2:
            if (nb1 == 0 && nb2 == 0 && nb3 == 0)
                return "deux-mille";
            else 
                return "deux-mille-"
        case 3:
            if (nb1 == 0 && nb2 == 0 && nb3 == 0)
                return "trois-mille";
            else 
                return "trois-mille-"
        case 4:
            if (nb1 == 0 && nb2 == 0 && nb3 == 0)
                return "quatre-mille";
            else 
                return "quatre-mille-"
        case 5:
            if (nb1 == 0 && nb2 == 0 && nb3 == 0)
                return "cinq-mille";
            else 
                return "cinq-mille-"
        case 6:
            if (nb1 == 0 && nb2 == 0 && nb3 == 0)
                return "six-mille";
            else 
                return "six-mille-"
        case 7:
            if (nb1 == 0 && nb2 == 0 && nb3 == 0)
                return "sept-mille";
            else 
                return "sept-mille-"
        case 8:
            if (nb1 == 0 && nb2 == 0 && nb3 == 0)
                return "huit-mille";
            else 
                return "huit-mille-"
        case 9:
            if (nb1 == 0 && nb2 == 0 && nb3 == 0)
                return "neuf-mille";
            else 
                return "neuf-mille-"
    }
}

function centaines(nb,nb1,nb2) 
{
    nb = parseInt(nb);
    switch (nb) {
        case 0:
            return "";
        case 1:
            if (nb1 == 0 && nb2 == 0)
                return "cent";
            else
                return "cent-";
        case 2:
            if (nb1 == 0 && nb2 == 0)
                return "deux-cent";
            else
                return "deux-cent-";
        case 3:
            if (nb1 == 0 && nb2 == 0)
                return "trois-cent";
            else
                return "trois-cent-";
        case 4:
            if (nb1 == 0 && nb2 == 0)
                return "quatre-cent";
            else
                return "quatre-cent-";
        case 5:
            if (nb1 == 0 && nb2 == 0)
                return "cinq-cent";
            else
                return "cinq-cent-";
        case 6:
            if (nb1 == 0 && nb2 == 0)
                return "six-cent";
            else
                return "six-cent-";
        case 7:
            if (nb1 == 0 && nb2 == 0)
                return "sept-cent";
            else
                return "sept-cent-";
        case 8:
            if (nb1 == 0 && nb2 == 0)
                return "huit-cent";
            else
                return "huit-cent-";
        case 9:
            if (nb1 == 0 && nb2 == 0)
                return "neuf-cent";
            else
                return "neuf-cent-";
    }
}

function dizaines(nb, nb2) 
{
    nb = parseInt(nb);
    switch (nb) {
        case 0:
            return "";
        case 1:
            return nb_chiants(nb2);
        case 2:
            if (nb2 == 0)
            {
                visiter = true;
                return "vingts";
            }
            if (nb2 == 1)
            {
                return "vingt-et-";
            }
            return "vingt-";  
        case 3:
            if (nb2 == 0) {
                visiter = true;
                return "trente";
            }
            if (nb2 == 1) {
                return "trentes-et-";
            }
            return "trentes-"
        case 4:
            if (nb2 == 0) {
                visiter = true;
                return "quarante";
            }
            if (nb2 == 1) {
                return "quarante-et-";
            }
            return "quarante-"
        case 5:
            if (nb2 == 0) {
                visiter = true;
                return "cinquante";
            }
            if (nb2 == 1) {
                return "cinquante-et-";
            }
            return "cinquante-";
        case 6:
            if (nb2 == 0) {
                visiter = true;
                return "soixante";
            }
            if (nb2 == 1) {
                return "soixante-et-";
            }
            return "soixante-";
        case 7:
            if (nb2 == 0) {
                visiter = true;
                return "soixante-dix";
            }
            if (nb2 == 1) {
                return "soixante-et-" + nb_chiants(nb2);;
            }
            return "soixante-" + nb_chiants(nb2);
        case 8:
            if (nb2 == 0) {
                visiter = true;
                return "quatre-vingts";
            }
            return "quatre_vingts-";
        case 9:
            if (nb2 == 0) {
                visiter = true;
                return "quatre-vingts-dix";
            }
            return "quatre-vingts-" + nb_chiants(nb2);
    }
}

function nb_chiants(nb) 
{
    nb = parseInt(nb);
    visiter = true;
    switch (nb) {
        case 0:
            return "dix";
        case 1:
            return "onze";
        case 2:
            return "douze";
        case 3:
            return "treize";
        case 4:
            return "quatorze";
        case 5:
            return "quinze";
        case 6:
            return "seize";
        default:
            visiter = false;
            return "dix-";
    }
}

function unite(nb) 
{
    nb = parseInt(nb);
    switch (nb) {
        case 0:
            return "";
        case 1:
            return "un";
        case 2:
            return "deux";
        case 3:
            return "trois";
        case 4:
            return "quatre";
        case 5:
            return "cinq";
        case 6:
            return "six";
        case 7:
            return "sept";
        case 8:
            return "huit";
        case 9:
            return "neuf";
    }
}

function start()
{
    while (true)
    {
        var nb = saisie_nb_entre_bornes();
        if (nb == -1)
            return -1;
        var n_txt = nb.toString();
        var tab = n_txt.split('');
        var ok = true;
        for (i = 0; i < tab.length; i++) {
            if (tab[i] == "," || tab[i] == ".") {
                ok = false;
            }
        }
        if (ok)
            break;
    }
    

    switch (tab.length) {
        case 1:
            var nom_unite = unite(tab[0]);
            alert(nom_unite);
            break;
        case 2:
            var nom_dizaines = dizaines(tab[0], tab[1]);
            if (!visiter) {
                var nom_unite = unite(tab[1]);
                alert(nom_dizaines + nom_unite);
            }
            else {
                alert(nom_dizaines);
            }
            break;
        case 3:
            var nom_centaines = centaines(tab[0], tab[1], tab[2]);
            var nom_dizaines = dizaines(tab[1], tab[2]);
            if (!visiter) {
                var nom_unite = unite(tab[2]);
                alert(nom_centaines + nom_dizaines + nom_unite);
            }
            else {
                alert(nom_centaines + nom_dizaines);
            }
            break;
        case 4:
            var nom_miliers = miliers(tab[0],tab[1],tab[2],tab[3]);
            var nom_centaines = centaines(tab[1],tab[2], tab[3]);
            var nom_dizaines = dizaines(tab[2], tab[3]);
            if (!visiter) {
                var nom_unite = unite(tab[3]);
                alert(nom_miliers + nom_centaines + nom_dizaines + nom_unite);
            }
            else {
                alert(nom_miliers + nom_centaines + nom_dizaines);
            }
            break;
    }
}

var visiter = false;
if (start() != -1)
{
    while (true) {
        if (confirm('Voulez-vous réessayer ?')) 
        {
            if (start() == -1)
                break;
        }
        else 
        {
            break;
        }
    }
}
alert("Merci pour votre essai !\nN'hésitez pas à me faire remonter d'éventuels bugs!");
window.close();
    



    